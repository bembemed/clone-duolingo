import React, {useEffect, useState} from "react";
import {  View, Alert, ActivityIndicator,} from "react-native";
import styles from "./App.styles";
import question from './assets/data/allQuestions'
import ImageMulatipleChoiceQuestions from "./src/components/ImageMultipleChoiceQuestion";
import OpenEndedQuestion from "./src/components/OpenEndedQuestion/OpenEndedQuestion";
import header from "./src/components/Header/header";
import Header from "./src/components/Header/header";
import AsyncStorage from "@react-native-async-storage/async-storage";
import FillInTheBlank from "./src/components/fillInTheBlank/fillInTheBlank";



export default App = () => {
  const [currentQuestionIndex,setcurrentQuestionIndex]= useState(0)
  const [currentQuestion, setcurrentQuestion] = useState(question[currentQuestionIndex])
  const [lives, setLives] = useState(5)
  const [hasloaded, setHasloaded] = useState(false)
  useEffect(()=>{
    if(currentQuestionIndex >= question.length){
      Alert.alert('you Won')
      setcurrentQuestionIndex(0)
      setLives(5)
    } else {
      setcurrentQuestion(question[currentQuestionIndex])
    }
    
  },[currentQuestionIndex])

  useEffect(()=>{
    loadData()
  },[])

  useEffect(()=>{
    if(hasloaded){
      saveData()
    }
  },[lives,currentQuestionIndex, hasloaded])
  const  onCorrect =()=>{
    setcurrentQuestionIndex(currentQuestionIndex+1)
  }

  const restart = ()=>{
    setLives(5),
    setcurrentQuestionIndex(0)
  }
  const onWrong = ()=>{
    if(lives<=1){
      Alert.alert("Game Over", "try again",[
        {
          text: "try again",
          onPress: restart
        }
      ])
    } else{
      setLives(lives-1)
    }
    
  }

  const saveData = async() =>{
    await AsyncStorage.setItem('lives ', lives.toString())
    await AsyncStorage.setItem('currentQuestionIndex ', currentQuestionIndex.toString())
  }

  const loadData = async ()=>{
    const loadedLives = await AsyncStorage.getItem('lives')
    if(loadedLives){
      setLives(parseInt(loadedLives))
    }
    const loadedcurrentIndex = await AsyncStorage.getItem('currentQuestionIndex')
    if(loadedcurrentIndex){
      setcurrentQuestionIndex(parseInt(loadedcurrentIndex))
    }
    setHasloaded(true)
  }

  if(!hasloaded){
    return (<ActivityIndicator />)
  }
  return (
      <View style={styles.root}>
        <Header progress={currentQuestionIndex/question.length}  lives={lives}/>
        
        {currentQuestion.type == "FILL_IN_THE_BLANK" && <FillInTheBlank 
          question={currentQuestion}
          onCorrect={onCorrect}
          onWrong={onWrong}
        />}
        { currentQuestion.type == "IMAGE_MULTIPLE_CHOICE" &&(<ImageMulatipleChoiceQuestions 
          question={currentQuestion}
          onCorrect={onCorrect}
          onWrong={onWrong}
          />
          )
        }

        { currentQuestion.type == 'OPEN_ENDED' && (<OpenEndedQuestion 
          question={currentQuestion}
          onCorrect={onCorrect}
          onWrong={onWrong}
        />
        )
        }
      </View>
  )
}

