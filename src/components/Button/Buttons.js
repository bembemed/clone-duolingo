import React from 'react'
import { Text,Pressable} from 'react-native'
import styles from './styles'
import PropTypes  from 'prop-types'
export default Buttons = ({text, onPress,disabled})=>{
    return (
        <Pressable 
            onPress={onPress} 
            style={[styles.container, disabled? styles.disabledContainer: {}]}
            disabled={disabled}
        >
            <Text style={styles.text}>{text}</Text>
        </Pressable>
    )
}


Buttons.propTypes={
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func,
    disabled: PropTypes.bool
}

Buttons.defaultProps = {
    onPress: ()=> {},
    disabled: false
  }