import React, {useEffect, useState} from "react";
import { Text, View } from "react-native";

import  ImageOptions  from '../imageOption/imageOptions'
import Buttons   from '../Button/Buttons'
import styles from './styles'
import  Proptypes from "prop-types";


export default ImageMultipleChoiceQuestion = ({question,onCorrect,onWrong}) =>{

  const [selected, setSelected] = useState(null);

    const onButtonPress = ()=>{
        if(selected.correct){
            onCorrect()
            setSelected(null)
        }else{
            onWrong()
          
        }
    }
    return (
        <>
            <Text style={styles.title}>{question.question} </Text>
            <View style={styles.optionsContainer}>
            {question.options.map((option)=>(
                <ImageOptions 
                    key={option.id}
                    image={option.image}
                    text={option.text}
                    isSelected={selected?.id == option.id}
                    onPress={()=> setSelected(option)}
                />
                )
            )}
            </View>

            <Buttons text="Check" onPress={onButtonPress} disabled={!selected}/>
        
        </>
    );


}



ImageMultipleChoiceQuestion.proptypes = {
    question: Proptypes.shape({
        question: Proptypes.string.isRequired,
        options: Proptypes.arrayOf(
            Proptypes.shape({
                id: Proptypes.string,
                text: Proptypes.string,
                image: Proptypes.string,
                correct: Proptypes.bool
            })
        ).isRequired
    }).isRequired
}