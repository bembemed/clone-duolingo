import React ,{useState} from 'react'
import {Text, View,Image, TextInput} from 'react-native'
import styles from './styles'
import mascot from '../../../assets/images/mascot.png'
import Buttons from '../Button/Buttons'
export default OpenEndedQuestion = ({question,onCorrect,onWrong}) => {
    const [input, setInput] = useState('')
    const onButtonPress = () =>{
        
        if(question.answer .toLowerCase().trim() === input.toLowerCase().trim()){
            onCorrect();
            
        }else{
            onWrong();
        }

        setInput('')
    }
    return(
        <>
            <Text style={styles.title}>translate this sentence </Text>
            <View style={styles.row}>
                <Image 
                    source={mascot}
                    style={styles.mascot}
                    resizeMode='contain'
                />

                <View style={styles.sentenceContainer}>
                    <Text style={styles.senntence}> {question.text}</Text>
                </View>


                
            </View>
            <TextInput 
                placeholder='type in english'
                style={styles.textInput}
                value={input}
                onChangeText={setInput}
                textAlignVertical='top'
                multiline

            />

            <Buttons text="Check" onPress={onButtonPress} disabled={!input}/>

        </>
    )
}