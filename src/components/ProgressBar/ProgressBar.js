import React from 'react'
import styles from './styles'
import { Text, View } from 'react-native'

export default ProgressBar = ({progress}) => {
    return (
        <View style={styles.bg}>
            <View style={[styles.fg, {width:`${progress*100}%`}]}>
                <View style={styles.heighlight} />
                    
                
            </View>
        </View>
    )
}