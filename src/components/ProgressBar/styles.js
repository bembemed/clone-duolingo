import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({
    bg:{
        backgroundColor: 'lightgrey',
        height: 30,
        borderRadius: 15,
        flex: 1,
    },
    fg:{
        flex:1,
        backgroundColor: '#FAC800',
        borderRadius:15,
    },
    heighlight:{
        backgroundColor:"#FAD131",
        // backgroundColor: "red",
        width:"90%",
        height:5,
        borderRadius:5,
        marginTop:5,
        alignSelf: 'center'
    }
})