import React from 'react';
import {View, StyleSheet, Text, Pressable} from 'react-native';


const WordOptions = ({text,onpress,isSelected})=>{
    return (
    <Pressable 
        onPress={onpress}
        style={[
            styles.root, 
            {backgroundColor:isSelected?"lightgray":"white"},
        ]}
    >
        <Text style={[styles.text,{color:isSelected?"lightgray":"black"}]}> {text}</Text>
    </Pressable>)
}


const styles = StyleSheet.create({
    root:{
        borderWidth:2,
        borderBottomWidth:4,
        borderColor:"lightgrey",
        borderRadius:5,
        padding:10,
        paddingHorizontal:15,
        margin:10
    },
    text:{}
})

export default WordOptions;
