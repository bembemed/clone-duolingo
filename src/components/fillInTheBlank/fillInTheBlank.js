import {React, useState} from 'react'
import styles from './styles'
import { Alert, Text, View } from 'react-native'
import Buttons from '../Button/Buttons'
import WordOptions from './WordOptions/WordOptions'


export default FillIntheBlank = ({question,onCorrect,onWrong}) =>{
    const [parts,setParts] = useState(question.parts)
    const onButtonPress = ()=>{
        if(checkIfCorrect()){
            onCorrect()
        } else{
            onWrong()
            Alert.alert("wrooong")
        }
    }
    const checkIfCorrect = ()=>{
        return parts.filter(part => part.isBlank && part.selected != part.text).length == 0
    }
    const addOptionToSelected = (option)=>{
        if( isSelected(option)){
            return;
        }
        const newParts = [...parts]
        for (let i =0; i <newParts.length; i++){
            if(newParts[i].isBlank && !newParts[i].selected){
                newParts[i].selected = option
                break;
            }
            setParts(newParts)
        }
    }

    const removeSelectedAt = (index)=>{
        const newParts= [...parts]
        newParts[index].selected = null
        setParts(newParts)
    }

    const isSelected= (option)=>{
        return (parts.filter(part =>part.isBlank && part.selected == option).length > 0)
    }

    const isReadyToCheck = ()=>{
        return (parts.filter(part =>part.isBlank && !part.selected ).length > 0) 
    }
    return (
        <>
            <Text style={styles.title}>Complete the sentence</Text>
            <View style={styles.row}>
                {parts.map((part, index)=>{
                    if(part.isBlank){
                        return (
                            <View key={index} style={styles.blank}>
                                {part.selected && <WordOptions 
                                    text={part.selected} 
                                    onpress={()=>removeSelectedAt(index)}
                                />}
                            </View>
                        )
                    }else{
                        return(<Text key={index} style={styles.text}>{part.text} </Text>)
                    }
                })}
                
                
            </View>

            <View style={styles.optionsContainer}>
                {question.options.map((option,k)=>{
                    return (
                        <WordOptions 
                            key={k}
                            text={option} 
                            isSelected={isSelected(option)}
                            onpress={()=> addOptionToSelected(option)}
                        />)
                })}
                
                
            </View>

            <Buttons text="Check" onPress={onButtonPress} disabled={isReadyToCheck()}/>

        </>
    )
}