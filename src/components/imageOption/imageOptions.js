import React from "react";
import { View, Image, Text,Pressable} from 'react-native'
import styles from "./styles";
import PropTypes from 'prop-types';

const  ImageOptions = ({ image, text,isSelected, onPress}) => {
    return (
      <Pressable 
        onPress={onPress}
        style={[styles.options, isSelected? styles.selectedContainer: {}]}>
              <Image 
                source={{
                  uri: image
                  }} 
                resizeMode="contain"
                style={styles.optionImage}  
              />
              
              <Text style={isSelected ? styles.selectedText: styles.optionText}>{text.toUpperCase()}</Text>
            </Pressable>
    )
}



ImageOptions.propTypes={
  image: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  isSelected: PropTypes.bool,
  onPress: PropTypes.func
}

ImageOptions.defaultProps = {
  text: "default",
  isSelected: false,
  onPress: ()=> {}
}

export default  ImageOptions;