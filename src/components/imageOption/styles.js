import {StyleSheet} from 'react-native'
const styles = StyleSheet.create({
    
    options:{
      //border 
      borderWidth:2,
      borderBottomWidth: 4,
      borderColor:"lightgrey",
      borderRadius: 10,

      //size
      width:"48%",
      height:"48%",

      padding:10,
      alignItems: "center"
    },
    selectedContainer:{
      backgroundColor: "#DDF4FE",
      borderColor: "#81D5FE",

    },
    optionImage:{
      width:"100%",
      flex:1
      
    },
    optionText:{
      fontWeight: "bold"
    },
    selectedText:{
      color: "#40BEF7",
      fontWeight: "bold"
    }
  })

export default styles